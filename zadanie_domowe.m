%MATLAB 2020b
%name:ZAD_1
%author:marcban311
%date: 14.11.2020
%version: v1.0

%exc_1
%defining planc 
h=6.62607015e-34 
exc_1= h/(2*pi)

%exc_2
e=exp(1)
exc_2=sin(pi/(4*e))

%exc_3
exc_3=hex2dec('0x0098d6')/1.445e23

%exc_4
exc_4=sqrt(e-pi)

%exc_5
%converting pi to string 
piStr=num2str(pi,15);
exc_5= piStr(14)

%exc_6
date = datetime('now');
birthDay= datetime(2000, 01, 31, 16, 31, 23);
exc_6= date-birthDay

%exc_7
R=6.37e6 
exc_6=atan(e^(sqrt(7)/2-log(R/10e8))/hex2dec('0xaaff'))

%exc_8
Na=6.02214076e23
exc_8=Na/4*1e-6

%exc_9
exc_9=2*exc_8/909*1000/exc_8